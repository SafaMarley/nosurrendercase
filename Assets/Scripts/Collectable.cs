using Managers;
using UnityEngine;

public class Collectable : MonoBehaviour
{
    private int _scoreCollectable = 10;
    public int ScoreCollectable => _scoreCollectable;

    private MeshRenderer _meshRenderer;
    private BoxCollider _boxCollider;

    private void Awake()
    {
        _meshRenderer = GetComponent<MeshRenderer>();
        _boxCollider = GetComponent<BoxCollider>();
    }

    public void GetCollected()
    {
        StartCoroutine(CollectableManager.Instance.DeActivateCollectable(this));
    }

    public void Disappear()
    {
        _meshRenderer.enabled = false;
        _boxCollider.enabled = false;
    }

    public void Reappear()
    {
        _meshRenderer.enabled = true;
        _boxCollider.enabled = true;
    }
}
