using UnityEngine;
using UnityEngine.SceneManagement;

namespace Managers
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private Camera mainCamera;
        [SerializeField] private Transform playerTransform;
        [SerializeField] private Transform[] entityTransforms;
        [SerializeField] private Transform entityTransformParent;
        [SerializeField] private Transform[] collectableTransforms;
        [SerializeField] private Transform collectableTransformParent;
        [SerializeField] private GameObject playerGO;
        [SerializeField] private GameObject collectableGO;
        [SerializeField] private GameObject botGO;
        private Vector3 _cameraOffset = new Vector3(0.0f, 5.0f, -10.0f);

        public static GameManager Instance { get; private set; }
        private void Awake()
        {
            Instance = this;
        }
        
        private void Start()
        {
            InitializeGame();
        }

        private void InitializeGame()
        {
            for (int i = 0; i < collectableTransforms.Length; i++)
            {
                Instantiate(collectableGO, collectableTransforms[i].position, Quaternion.identity, collectableTransformParent);
            }

            for (int i = 0; i < entityTransforms.Length; i++)
            {
                Instantiate(botGO, entityTransforms[i].position, Quaternion.identity, entityTransformParent);
            }

            mainCamera.transform.parent = Instantiate(playerGO, playerTransform.position, playerTransform.rotation).transform;
            mainCamera.transform.localPosition = _cameraOffset;
            
            BotManager.Instance.InitializeEntities();
            CollectableManager.Instance.InitializeCollectables();
        }

        public void ResetGame(Entity winner)
        {
            Destroy(winner.gameObject);
            //InitializeGame();
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}
