using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Managers
{
    public class CollectableManager : MonoBehaviour
    {
        private List<Collectable> _validCollectables;
        public static CollectableManager Instance { get; private set; }

        private void Awake()
        {
            Instance = this;
        }
        
        public void InitializeCollectables()
        {
            _validCollectables = FindObjectsOfType<Collectable>().ToList();
        }

        public IEnumerator DeActivateCollectable(Collectable collectable, float duration = 5.0f)
        {
            _validCollectables.Remove(collectable);
            collectable.Disappear();
            yield return new WaitForSeconds(duration);
            collectable.Reappear();
            _validCollectables.Add(collectable);
        }

        public Transform GetValidCollectable()
        {
            if (_validCollectables.Count > 0)
            {
                return _validCollectables[0].gameObject.transform;
            }
            else
            {
                return null;
            }
        }
    }
}
