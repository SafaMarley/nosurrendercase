using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Managers
{
    public class BotManager : MonoBehaviour
    {
        public static BotManager Instance { get; private set; }
        private List<Entity> _entities;

        public Entity Entity
        {
            get => _entities[0];
        }

        private void Awake()
        {
            Instance = this;
        }

        public void InitializeEntities()
        {
            _entities = FindObjectsOfType<Entity>().ToList();
        }

        public Transform FindClosestEnemy(Entity currentEntity)
        {
            Transform closestGoal = null;
            float smallestDistance = float.MaxValue;
            foreach (var entity in _entities)
            {
                if (currentEntity.transform == entity.transform)
                {
                    continue;
                }

                if (currentEntity.ScorePlayer > entity.ScorePlayer)
                {
                    float distance = Vector3.Distance(currentEntity.transform.position, entity.transform.position);
                    if (distance < smallestDistance && distance > .5f)
                    {
                        smallestDistance = distance;
                        closestGoal = entity.transform;
                    }
                }
            }

            if (closestGoal is null)
            {
                closestGoal = CollectableManager.Instance.GetValidCollectable();
            }
            return closestGoal;
        }

        public void RemoveEntityFromList(Entity entity)
        {
            _entities.Remove(entity);
            StartCoroutine(entity.CountdownForDeath());
            if (_entities.Count == 1)
            {
                GameManager.Instance.ResetGame(_entities[0]);
            }
        }
    }
}
