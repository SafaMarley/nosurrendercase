namespace Enums
{
    public enum State
    {
        Idle = 0,
        Charging = 1,
        Knocked = 2
    }
}