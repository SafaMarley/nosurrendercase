using System.Collections;
using Enums;
using UnityEngine;

public class Entity : MonoBehaviour
{
    protected float Speed = 5.0f;
    protected Rigidbody Rigidbody;
    protected State State;
    
    private int _scorePlayer;
    public int ScorePlayer => _scorePlayer;

    private void Awake()
    {
        Rigidbody = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        if (State == State.Charging)
        {
            Move();
        }
    }

    protected virtual void Move()
    {
        
    }

    private void OnCollisionStay(Collision other)
    {
        if (other.gameObject.CompareTag("Sumo"))
        {
            Entity otherSumo = other.gameObject.GetComponent<Entity>();
            int scoreDif = ScorePlayer - otherSumo.ScorePlayer;
            if (scoreDif > 0)
            {
                otherSumo.GetPushed(otherSumo.transform.position - transform.position, scoreDif);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Collectable"))
        {
            Collectable collectedObject = other.gameObject.GetComponent<Collectable>();
            
            _scorePlayer += collectedObject.ScoreCollectable;
            ScoreCounter.Instance.UpdateScore(_scorePlayer);
            collectedObject.GetCollected();
        }
    }

    public void ChangeState(State state)
    {
        switch (state)
        {
            case State.Idle:
                State = state;
                OnStateChangeIdle();
                break;
            
            case State.Charging:
                State = state;
                OnStateChangeCharge();
                break;

            case State.Knocked:
                State = state;
                OnStateChangeKnocked();
                break;
        }
    }

    protected virtual void OnStateChangeIdle()
    {
        
    }
    protected virtual void OnStateChangeCharge()
    {
        
    }
    protected virtual void OnStateChangeKnocked()
    {
        
    }

    public void GetPushed(Vector3 direction, int powerMultiplier)
    {
        ChangeState(State.Knocked);
        direction = direction.normalized;
        Rigidbody.AddForce(direction * powerMultiplier, ForceMode.Impulse);
    }

    public IEnumerator CountdownForDeath()
    {
        yield return new WaitForSeconds(5.0f);
        OnDeath();
    }

    public virtual void OnDeath()
    {
        
    }
}
