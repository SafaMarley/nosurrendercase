using Enums;
using Managers;
using UnityEngine;

public class Bot : Entity
{
    private void Start()
    {
        State = State.Idle;
    }

    protected override void Move()
    {
        Transform transformRef = transform;
        transform.LookAt(BotManager.Instance.FindClosestEnemy(this).position);
        transformRef.position += transformRef.forward * (Speed * Time.deltaTime);
    }

    private void Update()
    {
        Transform moveTowards = BotManager.Instance.FindClosestEnemy(this);
        if (moveTowards)
        {
            if (State != State.Charging)
            {
                ChangeState(State.Charging);
            }
        }
        else
        {
            if (State != State.Idle)
            {
                ChangeState(State.Idle);
            }
        }
    }

    protected override void OnStateChangeIdle()
    {
        Rigidbody.velocity = Vector3.zero;
    }

    protected override void OnStateChangeCharge()
    {
        base.OnStateChangeCharge();
    }

    protected override void OnStateChangeKnocked()
    {
        base.OnStateChangeKnocked();
    }

    public override void OnDeath()
    {
        Destroy(gameObject);
    }
}
