using Enums;
using Managers;
using UnityEngine;
using UnityEngine.InputSystem;

public class Player : Entity
{
    private PlayerInput _playerInput;

    private void Start()
    {
        _playerInput = GetComponent<PlayerInput>();
        State = State.Charging;
    }
    
    
    protected override void Move()
    {
        Vector2 inputMovement = _playerInput.actions["Move"].ReadValue<Vector2>();

        Transform tempRef = transform;
        Vector3 move = tempRef.forward * inputMovement.y + tempRef.right * inputMovement.x;
        tempRef.position += move * (Time.deltaTime * Speed);
    }

    private void LateUpdate()
    {
        Vector2 inputRotation = _playerInput.actions["Rotate"].ReadValue<Vector2>();
        transform.Rotate(Vector3.up * inputRotation.x);
    }

    public override void OnDeath()
    {
        Transform camera = transform.GetChild(0);
        camera.parent = BotManager.Instance.Entity.transform;
        camera.localPosition = new Vector3(0.0f, 5.0f, 10.0f);
        camera.localRotation = Quaternion.Euler(25.0f, 180.0f, 0.0f);
        Destroy(gameObject);
    }
}
