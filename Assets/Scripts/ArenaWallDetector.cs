using Managers;
using UnityEngine;

public class ArenaWallDetector : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Sumo"))
        {
            BotManager.Instance.RemoveEntityFromList(other.GetComponent<Entity>());
        }
    }
}
