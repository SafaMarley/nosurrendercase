using TMPro;
using UnityEngine;

public class ScoreCounter : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI scoreText;
    public static ScoreCounter Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
    }
    

    public void UpdateScore(int newScore)
    {
        scoreText.text = "Score: " + newScore;
    }
}
